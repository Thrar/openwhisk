# Customize configuration of action containers

The invoker is in charge of creating containers to run actions.

Eventually, it calls into modules in [`core/invoker/src/main/scala/org/apache/openwhisk/core/containerpool/`](../../core/invoker/src/main/scala/org/apache/openwhisk/core/containerpool/), depending on the backend:

* using Docker (raw containers): [`DockerContainerFactory.scala`](../../core/invoker/src/main/scala/org/apache/openwhisk/core/containerpool/docker/DockerContainerFactory.scala)
* using Kubernetes (containers in pods): [`WhiskPodBuilder.scala`](../../core/invoker/src/main/scala/org/apache/openwhisk/core/containerpool/kubernetes/WhiskPodBuilder.scala), method `buildPodSpec()`
